class Blog{
    constructor(id,nom, prenom, desc, annee){
        this.id = id;
        this.nom = nom;
        this.prenom = prenom; 
        this.desc = desc; 
    }
}

const data = [   
    new Blog( 0, "Article ","1","Lorem ipsum"),
    new Blog( 1, "Article","2","Lorem ipsum"),
    new Blog( 2, "Article","3","Lorem ipsum"), 
    new Blog( 3, "Article","4","Lorem ipsum"),
    new Blog( 4, "Article","5","Lorem ipsum"),
    new Blog( 5, "Article","6","Lorem ipsum"),
    new Blog( 6, "Article","7","Lorem ipsum"),
    new Blog( 7, "Article","8","Lorem ipsum"),
    new Blog( 8, "Article","9","Lorem ipsum"),
    new Blog( 9, "Article","10","Lorem ipsum"),
];

exports.getBlogById = function(id){
   return data[id]; 
}

exports.addBlog = function(dat){
    let id = data.length;
    data.push(new Blog(id, dat.nom, dat.prenom, dat.description))
    return true;
}

exports.allBlog = data;