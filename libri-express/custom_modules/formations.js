class Formations{
    constructor(id,nom, desc, prix){
        this.id = id;
        this.nom = nom;
        this.desc = desc; 
        this.prix = prix;
    }
}

const data = [   
    new Formations( 0,"Formation 1","Formation de MongoDB","200$"),
    new Formations( 1,"Formation 2","Formation de MongoDB","300$"),
    new Formations( 2,"Formation 3","Formation de MongoDB","354$"), 
    new Formations( 3,"Formation 4","Formation de MongoDB","154$"),
    new Formations( 4,"Formation 5","Formation de MongoDB","202$"),
    new Formations( 5,"Formation 1","Formation de NodeJs","200$"),
    new Formations( 6,"Formation 2","Formation de NodeJs","300$"),
    new Formations( 7,"Formation 3","Formation de NodeJs","354$"), 
    new Formations( 8,"Formation 4","Formation de NodeJs","154$"),
    new Formations( 9,"Formation 5","Formation de NodeJs","202$"),
];

exports.getFormationById = function(id){
   return data[id]; 
}

exports.addFormation = function(dat){
    let id = data.length;
    data.push(new Formations(id, dat.nom, dat.description, dat.prix))
    return true;
}

exports.allFormations = data;