var express = require('express');
var router = express.Router();
let formations= require("../custom_modules/formations.js")
let blog= require("../custom_modules/blog.js")


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Accueil', description:"Accueil du projet" });
});

router.get('/Formations', function(req, res, next){
  res.render('Formations',  { title: 'Les Formations', description:"Formation MongoDB et NodeJS", formations: formations.allFormations });
});

router.get('/Blog', function(req, res, next){
  res.render('Blog',  { title: 'Le Blog', description:"10 articles", blog: blog.allBlog });
});

router.get('/Contact', function(req, res, next){
  res.render('Contact',  { title: 'Nous contacter', description:"formulaire" });
});

router.get('/Formations/:id', function(req, res, next){
  res.render('Forma',  { forma: formations.getFormationById(req.params.id)});
});

router.get('/Contact/add', function(req,res,next){
  res.render('add');
})

router.post('/Contact/add', function(req,res,next){
  let demande = formations.addFormation(req,body)
  if(demande){
    res.render('Formations',  { title: 'Les Formations', description:"Formation MongoDB et NodeJS", formations: formations.allFormations });
  }
}) 
module.exports = router;
